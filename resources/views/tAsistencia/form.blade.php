@extends('layouts.app')

@section('template_title')
Asistencia
@endsection

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <div style="display: flex; justify-content: space-between; align-items: center;">

                        <span id="card_title">
                            {{ __('Asistencia') }}
                        </span>

                        <div class="float-right">
                            <a href="{{ route('asistencia.create') }}" class="btn btn-primary btn-sm float-right" data-placement="left">
                                {{ __('Create New') }}
                            </a>
                        </div>
                    </div>
                </div>
                @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <p>{{ $message }}</p>
                </div>
                @endif

                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-hover">
                            <thead class="thead">
                                <tr>
                                    <th>No</th>

                                    <th>Nombre Aprendiz</th>
                                    <th>Asiste</th>
                                    <th>Programa</th>
                                    <th>Instructor</th>

                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($tAprendizs as $tAprendiz)
                                <tr>
                                    <td>{{ $tAprendiz->id }}</td>

                                    <td>{{ $tAprendiz->NombreAprendiz }}</td>
                                    <td>
                                        <select name="Asistencia">
                                            <option value="X">X</option>
                                            <option value="A">A</option>
                                            <option value="E">E</option>
                                            <option value="R">R</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select name="Asistencia">
                                            @foreach ($tProgramas as $tPrograma)
                                            <option value="{{ $tPrograma->id }}">{{ $tPrograma->Nombre_Programa }}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td>
                                        <select name="Asistencia">
                                            @foreach ($tInstructors as $tInstructor)
                                            <option value="{{ $tInstructor->id }}">{{ $tInstructor->NombreInstructor }}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td>
                                        <form action="{{ route('tAsistencia.destroy',$tAprendiz->id) }}" method="POST">
                                            <a class="btn btn-sm btn-primary " href="{{ route('tAsistencia.show',$tAprendiz->id) }}"><i class="fa fa-fw fa-eye"></i> Show</a>
                                            <a class="btn btn-sm btn-success" href="{{ route('tAsistencia.edit',$tAprendiz->id) }}"><i class="fa fa-fw fa-edit"></i> Edit</a>
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-fw fa-trash"></i> Delete</button>
                                        </form>
                                    </td>

                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<div class="card-header">
    <div style="display: flex; justify-content: space-between; align-items: center;">
    <div class="box-footer mt20">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>

    </div>
</div>
</div>


</div>
</div>
</div>
</div>
@endsection
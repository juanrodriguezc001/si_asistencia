@extends('layouts.app')

@section('template_title')
    {{ $regAsistencia->name ?? 'Show Reg Asistencia' }}
@endsection

@section('content')
    <section class="content container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="float-left">
                            <span class="card-title">Show Reg Asistencia</span>
                        </div>
                        <div class="float-right">
                            <a class="btn btn-primary" href="{{ route('regAsistencia.index') }}"> Back</a>
                        </div>
                    </div>

                    <div class="card-body">
                        
                        <div class="form-group">
                            <strong>Nombreaprendiz:</strong>
                            {{ $regAsistencia->NombreAprendiz }}
                        </div>
                        <div class="form-group">
                            <strong>Asistencia:</strong>
                            {{ $regAsistencia->Asistencia }}
                        </div>
                        <div class="form-group">
                            <strong>Programa:</strong>
                            {{ $regAsistencia->Programa }}
                        </div>
                        <div class="form-group">
                            <strong>Instructor:</strong>
                            {{ $regAsistencia->Instructor }}
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

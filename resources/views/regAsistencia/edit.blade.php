@extends('layouts.app')

@section('template_title')
    Update Reg Asistencia
@endsection

@section('content')
    <section class="content container-fluid">
        <div class="">
            <div class="col-md-12">

                @includeif('partials.errors')

                <div class="card card-default">
                    <div class="card-header">
                        <span class="card-title">Update Reg Asistencia</span>
                    </div>
                    <div class="card-body">
                        <form method="POST" action="{{ route('regAsistencia.update', $regAsistencia->id) }}"  role="form" enctype="multipart/form-data">
                            {{ method_field('PATCH') }}
                            @csrf

                            @include('regAsistencia.form')

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@extends('layouts.app')

@section('template_title')
    Reg Asistencia
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <div style="display: flex; justify-content: space-between; align-items: center;">

                            <span id="card_title">
                                {{ __('Reg Asistencia') }}
                            </span>

                             <div class="float-right">
                                <a href="{{ route('regAsistencia.create') }}" class="btn btn-primary btn-sm float-right"  data-placement="left">
                                  {{ __('Create New') }}
                                </a>
                              </div>
                        </div>
                    </div>
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success">
                            <p>{{ $message }}</p>
                        </div>
                    @endif

                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-hover">
                                <thead class="thead">
                                    <tr>
                                        <th>No</th>
                                        
										<th>Nombreaprendiz</th>
										<th>Asistencia</th>
										<th>Programa</th>
										<th>Instructor</th>

                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($regAsistencias as $regAsistencia)
                                        <tr>
                                            <td>{{ ++$i }}</td>
                                            
											<td>{{ $regAsistencia->NombreAprendiz }}</td>
											<td>{{ $regAsistencia->Asistencia }}</td>
											<td>{{ $regAsistencia->Programa }}</td>
											<td>{{ $regAsistencia->Instructor }}</td>

                                            <td>
                                                <form action="{{ route('regAsistencia.destroy',$regAsistencia->id) }}" method="POST">
                                                    <a class="btn btn-sm btn-primary " href="{{ route('regAsistencia.show',$regAsistencia->id) }}"><i class="fa fa-fw fa-eye"></i> Show</a>
                                                    <a class="btn btn-sm btn-success" href="{{ route('regAsistencia.edit',$regAsistencia->id) }}"><i class="fa fa-fw fa-edit"></i> Edit</a>
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-fw fa-trash"></i> Delete</button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                {!! $regAsistencias->links() !!}
            </div>
        </div>
    </div>
@endsection

<div class="box box-info padding-1">
    <div class="box-body">
        
        <div class="form-group">
            {{ Form::label('NombreAprendiz') }}
            <br>
            <select name="Programa" class='form-control'>
                @foreach ($tAprendizs as $tAprendiz)
                <option value="{{ $tAprendiz->NombreAprendiz }}">{{ $tAprendiz->NombreAprendiz }}</option>
                @endforeach
            </select>
            <!--{{ Form::text('NombreAprendiz', $regAsistencia->NombreAprendiz, ['class' => 'form-control' . ($errors->has('NombreAprendiz') ? ' is-invalid' : ''), 'placeholder' => 'Nombreaprendiz']) }}
            {!! $errors->first('NombreAprendiz', '<div class="invalid-feedback">:message</p>') !!}-->
        </div>
        <div class="form-group">
            {{ Form::label('Asistencia') }}
            <br>
            <select name="Programa" class='form-control'>
            <option value="X">X</option>
            <option value="A">A</option>
            <option value="E">E</option>
            <option value="R">R</option>
            </select>
            <!--{{ Form::text('Asistencia', $regAsistencia->Asistencia, ['class' => 'form-control' . ($errors->has('Asistencia') ? ' is-invalid' : ''), 'placeholder' => 'Asistencia']) }}
            {!! $errors->first('Asistencia', '<div class="invalid-feedback">:message</p>') !!}-->
        </div>
        <div class="form-group">
            
            {{ Form::label('Programa') }}
            <br>
            <select name="Programa" class='form-control'>
                @foreach ($tProgramas as $tPrograma)
                <option value="{{ $tPrograma->Nombre_Programa }}">{{ $tPrograma->Nombre_Programa }}</option>
                @endforeach
            </select>
            <!--{{ Form::text('Programa', $regAsistencia->Programa, ['class' => 'form-control' . ($errors->has('Programa') ? ' is-invalid' : ''), 'placeholder' => 'Programa']) }}
            {!! $errors->first('Programa', '<div class="invalid-feedback">:message</p>') !!}-->
        </div>
        <div class="form-group">
            {{ Form::label('Instructor') }}
            <br>
            <select name="Programa" class='form-control'.($errors->has('Instructor') ? ' is-invalid' : '')>
                @foreach ($tInstructors as $tInstructor)
                <option value="{{ $regAsistencia->Instructor }}">{{ $tInstructor->NombreInstructor }}</option>
                @endforeach
            </select>
            <!--{{ Form::text('Instructor', $regAsistencia->Instructor, ['class' => 'form-control' . ($errors->has('Instructor') ? ' is-invalid' : ''), 'placeholder' => 'Instructor']) }}
            {!! $errors->first('Instructor', '<div class="invalid-feedback">:message</p>') !!}-->
        </div>

    </div>
    <div class="box-footer mt20">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</div>
@extends('layouts.app')

@section('template_title')
    Asistencia
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <div style="display: flex; justify-content: space-between; align-items: center;">

                            <span id="card_title">
                                {{ __('Asistencia') }}
                            </span>

                             <div class="float-right">
                                <a href="{{ route('asistencia.create') }}" class="btn btn-primary btn-sm float-right"  data-placement="left">
                                  {{ __('Create New') }}
                                </a>
                              </div>
                        </div>
                    </div>
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success">
                            <p>{{ $message }}</p>
                        </div>
                    @endif

                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-hover">
                                <thead class="thead">
                                    <tr>
                                        <th>No</th>
                                        
										<th>Nombre Aprendiz</th>
                                        <th>Asiste</th>
										<th>Fecha Asiste</th>
                                        <th>id Aprendiz</th>
										

                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($cAsistencias as $cAsistencia)
                                        <tr>
                                            <td>{{ $cAsistencia->id }}</td>
                                            
                                            <td>{{ $cAsistencia->nombreAprendiz }}</td>
											<td>{{ $cAsistencia->asiste }}</td>
											<td>{{ $cAsistencia->fecha_asiste }}</td>
                                            <td>{{ $cAsistencia->idAprendiz }}</td>

                                            <td>
                                                <form action="{{ route('cAsistencia.destroy',$cAsistencia->id) }}" method="POST">
                                                    <a class="btn btn-sm btn-primary " href="{{ route('cAsistencia.show',$cAsistencia->id) }}"><i class="fa fa-fw fa-eye"></i> Show</a>
                                                    <a class="btn btn-sm btn-success" href="{{ route('cAsistencia.edit',$cAsistencia->id) }}"><i class="fa fa-fw fa-edit"></i> Edit</a>
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-fw fa-trash"></i> Delete</button>
                                                </form>
                                            </td>

                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
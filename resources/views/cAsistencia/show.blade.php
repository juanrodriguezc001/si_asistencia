@extends('layouts.app')

@section('template_title')
    {{ $asistencia->name ?? 'Show Asistencia' }}
@endsection

@section('content')
    <section class="content container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="float-left">
                            <span class="card-title">Show Asistencia</span>
                        </div>
                        <div class="float-right">
                            <a class="btn btn-primary" href="{{ route('asistencia.index') }}"> Back</a>
                        </div>
                    </div>

                    <div class="card-body">
                    
                    <div class="form-group">
                            <strong>Nombre del aprendiz:</strong>
                            {{ $cAsistencia->nombreAprendiz }}
                        </div>
                        
                        <div class="form-group">
                            <strong>Asiste:</strong>
                            {{ $cAsistencia->asiste }}
                        </div>
                        <div class="form-group">
                            <strong>Fecha Asiste:</strong>
                            {{ $cAsistencia->fecha_asiste }}
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
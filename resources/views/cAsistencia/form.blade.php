<div class="box box-info padding-1">
    <div class="box-body">
        
        <div class="form-group">
            {{ Form::label('NombreAprendiz') }}
            {{ Form::text('NombreAprendiz', $cAsistencia->NombreAprendiz, ['class' => 'form-control' . ($errors->has('NombreAprendiz') ? ' is-invalid' : ''), 'placeholder' => 'Nombreaprendiz']) }}
            {!! $errors->first('NombreAprendiz', '<div class="invalid-feedback">:message</p>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('Asiste') }}
            {{ Form::text('Asiste', $cAsistencia->cAsistencia, ['class' => 'form-control' . ($errors->has('Asiste') ? ' is-invalid' : ''), 'placeholder' => 'Asiste']) }}
            {!! $errors->first('Asiste', '<div class="invalid-feedback">:message</p>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('Fecha_Asiste') }}
            {{ Form::text('Fecha_Asiste', $cAsistencia->Fecha_Asiste, ['class' => 'form-control' . ($errors->has('Fecha_Asiste') ? ' is-invalid' : ''), 'placeholder' => 'Fecha Asiste']) }}
            {!! $errors->first('Fecha_Asiste', '<div class="invalid-feedback">:message</p>') !!}
        </div>

    </div>
    <div class="box-footer mt20">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</div>
@extends('layouts.app')

@section('template_title')
    {{ $asistencia->name ?? 'Show Asistencia' }}
@endsection

@section('content')
    <section class="content container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="float-left">
                            <span class="card-title">Show Asistencia</span>
                        </div>
                        <div class="float-right">
                            <a class="btn btn-primary" href="{{ route('asistencia.index') }}"> Back</a>
                        </div>
                    </div>

                    <div class="card-body">
                        
                        <div class="form-group">
                            <strong>Asiste:</strong>
                            {{ $asistencia->Asiste }}
                        </div>
                        <div class="form-group">
                            <strong>Fecha Asiste:</strong>
                            {{ $asistencia->Fecha_Asiste }}
                        </div>
                        <div class="form-group">
                            <strong>Aprendizs Id:</strong>
                            {{ $asistencia->aprendizs_id }}
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

<div class="box box-info padding-1">
    <div class="box-body">
        
        <div class="form-group">
            {{ Form::label('Asiste') }}
            {{ Form::text('Asiste', $asistencia->Asiste, ['class' => 'form-control' . ($errors->has('Asiste') ? ' is-invalid' : ''), 'placeholder' => 'Asiste']) }}
            {!! $errors->first('Asiste', '<div class="invalid-feedback">:message</p>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('Fecha_Asiste') }}
            {{ Form::text('Fecha_Asiste', $asistencia->Fecha_Asiste, ['class' => 'form-control' . ($errors->has('Fecha_Asiste') ? ' is-invalid' : ''), 'placeholder' => 'Fecha Asiste']) }}
            {!! $errors->first('Fecha_Asiste', '<div class="invalid-feedback">:message</p>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('aprendizs_id') }}
            {{ Form::text('aprendizs_id', $asistencia->aprendizs_id, ['class' => 'form-control' . ($errors->has('aprendizs_id') ? ' is-invalid' : ''), 'placeholder' => 'Aprendizs Id']) }}
            {!! $errors->first('aprendizs_id', '<div class="invalid-feedback">:message</p>') !!}
        </div>

    </div>
    <div class="box-footer mt20">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</div>
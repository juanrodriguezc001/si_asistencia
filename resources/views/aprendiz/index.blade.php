@extends('layouts.app')

@section('template_title')
    Aprendiz
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <div style="display: flex; justify-content: space-between; align-items: center;">

                            <span id="card_title">
                                {{ __('Aprendiz') }}
                            </span>

                             <div class="float-right">
                                <a href="{{ route('aprendiz.create') }}" class="btn btn-primary btn-sm float-right"  data-placement="left">
                                  {{ __('Create New') }}
                                </a>
                              </div>
                        </div>
                    </div>
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success">
                            <p>{{ $message }}</p>
                        </div>
                    @endif

                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-hover">
                                <thead class="thead">
                                    <tr>
                                        <th>No</th>
                                        
										<th>Nombreaprendiz</th>
										<th>Apellidoaprendiz</th>
										<th>Tipodocumentoaprendiz</th>
										<th>Documentoaprendiz</th>
										<th>Asistencias Id</th>
										<th>Users Id</th>

                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($aprendizs as $aprendiz)
                                        <tr>
                                            <td>{{ ++$i }}</td>
                                            
											<td>{{ $aprendiz->NombreAprendiz }}</td>
											<td>{{ $aprendiz->ApellidoAprendiz }}</td>
											<td>{{ $aprendiz->TipoDocumentoAprendiz }}</td>
											<td>{{ $aprendiz->DocumentoAprendiz }}</td>
											<td>{{ $aprendiz->asistencias_id }}</td>
											<td>{{ $aprendiz->users_id }}</td>

                                            <td>
                                                <form action="{{ route('aprendiz.destroy',$aprendiz->id) }}" method="POST">
                                                    <a class="btn btn-sm btn-primary " href="{{ route('aprendiz.show',$aprendiz->id) }}"><i class="fa fa-fw fa-eye"></i> Show</a>
                                                    <a class="btn btn-sm btn-success" href="{{ route('aprendiz.edit',$aprendiz->id) }}"><i class="fa fa-fw fa-edit"></i> Edit</a>
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-fw fa-trash"></i> Delete</button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                {!! $aprendizs->links() !!}
            </div>
        </div>
    </div>
@endsection

<div class="box box-info padding-1">
    <div class="box-body">
        
        <div class="form-group">
            {{ Form::label('NombreAprendiz') }}
            {{ Form::text('NombreAprendiz', $aprendiz->NombreAprendiz, ['class' => 'form-control' . ($errors->has('NombreAprendiz') ? ' is-invalid' : ''), 'placeholder' => 'Nombreaprendiz']) }}
            {!! $errors->first('NombreAprendiz', '<div class="invalid-feedback">:message</p>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('ApellidoAprendiz') }}
            {{ Form::text('ApellidoAprendiz', $aprendiz->ApellidoAprendiz, ['class' => 'form-control' . ($errors->has('ApellidoAprendiz') ? ' is-invalid' : ''), 'placeholder' => 'Apellidoaprendiz']) }}
            {!! $errors->first('ApellidoAprendiz', '<div class="invalid-feedback">:message</p>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('TipoDocumentoAprendiz') }}
            {{ Form::text('TipoDocumentoAprendiz', $aprendiz->TipoDocumentoAprendiz, ['class' => 'form-control' . ($errors->has('TipoDocumentoAprendiz') ? ' is-invalid' : ''), 'placeholder' => 'Tipodocumentoaprendiz']) }}
            {!! $errors->first('TipoDocumentoAprendiz', '<div class="invalid-feedback">:message</p>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('DocumentoAprendiz') }}
            {{ Form::text('DocumentoAprendiz', $aprendiz->DocumentoAprendiz, ['class' => 'form-control' . ($errors->has('DocumentoAprendiz') ? ' is-invalid' : ''), 'placeholder' => 'Documentoaprendiz']) }}
            {!! $errors->first('DocumentoAprendiz', '<div class="invalid-feedback">:message</p>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('asistencias_id') }}
            {{ Form::text('asistencias_id', $aprendiz->asistencias_id, ['class' => 'form-control' . ($errors->has('asistencias_id') ? ' is-invalid' : ''), 'placeholder' => 'Asistencias Id']) }}
            {!! $errors->first('asistencias_id', '<div class="invalid-feedback">:message</p>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('users_id') }}
            {{ Form::text('users_id', $aprendiz->users_id, ['class' => 'form-control' . ($errors->has('users_id') ? ' is-invalid' : ''), 'placeholder' => 'Users Id']) }}
            {!! $errors->first('users_id', '<div class="invalid-feedback">:message</p>') !!}
        </div>

    </div>
    <div class="box-footer mt20">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</div>
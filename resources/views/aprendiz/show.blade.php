@extends('layouts.app')

@section('template_title')
    {{ $aprendiz->name ?? 'Show Aprendiz' }}
@endsection

@section('content')
    <section class="content container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="float-left">
                            <span class="card-title">Show Aprendiz</span>
                        </div>
                        <div class="float-right">
                            <a class="btn btn-primary" href="{{ route('aprendiz.index') }}"> Back</a>
                        </div>
                    </div>

                    <div class="card-body">
                        
                        <div class="form-group">
                            <strong>Nombreaprendiz:</strong>
                            {{ $aprendiz->NombreAprendiz }}
                        </div>
                        <div class="form-group">
                            <strong>Apellidoaprendiz:</strong>
                            {{ $aprendiz->ApellidoAprendiz }}
                        </div>
                        <div class="form-group">
                            <strong>Tipodocumentoaprendiz:</strong>
                            {{ $aprendiz->TipoDocumentoAprendiz }}
                        </div>
                        <div class="form-group">
                            <strong>Documentoaprendiz:</strong>
                            {{ $aprendiz->DocumentoAprendiz }}
                        </div>
                        <div class="form-group">
                            <strong>Asistencias Id:</strong>
                            {{ $aprendiz->asistencias_id }}
                        </div>
                        <div class="form-group">
                            <strong>Users Id:</strong>
                            {{ $aprendiz->users_id }}
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

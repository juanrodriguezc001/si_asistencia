<div class="box box-info padding-1">
    <div class="box-body">
        
        <div class="form-group">
            {{ Form::label('NombreInstructor') }}
            {{ Form::text('NombreInstructor', $instructor->NombreInstructor, ['class' => 'form-control' . ($errors->has('NombreInstructor') ? ' is-invalid' : ''), 'placeholder' => 'Nombreinstructor']) }}
            {!! $errors->first('NombreInstructor', '<div class="invalid-feedback">:message</p>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('ApellidoInstructor') }}
            {{ Form::text('ApellidoInstructor', $instructor->ApellidoInstructor, ['class' => 'form-control' . ($errors->has('ApellidoInstructor') ? ' is-invalid' : ''), 'placeholder' => 'Apellidoinstructor']) }}
            {!! $errors->first('ApellidoInstructor', '<div class="invalid-feedback">:message</p>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('TipoDocumentoInstructor') }}
            {{ Form::text('TipoDocumentoInstructor', $instructor->TipoDocumentoInstructor, ['class' => 'form-control' . ($errors->has('TipoDocumentoInstructor') ? ' is-invalid' : ''), 'placeholder' => 'Tipodocumentoinstructor']) }}
            {!! $errors->first('TipoDocumentoInstructor', '<div class="invalid-feedback">:message</p>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('DocumentoInstructor') }}
            {{ Form::text('DocumentoInstructor', $instructor->DocumentoInstructor, ['class' => 'form-control' . ($errors->has('DocumentoInstructor') ? ' is-invalid' : ''), 'placeholder' => 'Documentoinstructor']) }}
            {!! $errors->first('DocumentoInstructor', '<div class="invalid-feedback">:message</p>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('users_id') }}
            {{ Form::text('users_id', $instructor->users_id, ['class' => 'form-control' . ($errors->has('users_id') ? ' is-invalid' : ''), 'placeholder' => 'Users Id']) }}
            {!! $errors->first('users_id', '<div class="invalid-feedback">:message</p>') !!}
        </div>

    </div>
    <div class="box-footer mt20">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</div>
@extends('layouts.app')

@section('template_title')
    Instructor
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <div style="display: flex; justify-content: space-between; align-items: center;">

                            <span id="card_title">
                                {{ __('Instructor') }}
                            </span>

                             <div class="float-right">
                                <a href="{{ route('instructor.create') }}" class="btn btn-primary btn-sm float-right"  data-placement="left">
                                  {{ __('Create New') }}
                                </a>
                              </div>
                        </div>
                    </div>
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success">
                            <p>{{ $message }}</p>
                        </div>
                    @endif

                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-hover">
                                <thead class="thead">
                                    <tr>
                                        <th>No</th>
                                        
										<th>Nombreinstructor</th>
										<th>Apellidoinstructor</th>
										<th>Tipodocumentoinstructor</th>
										<th>Documentoinstructor</th>
										<th>Users Id</th>

                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($instructors as $instructor)
                                        <tr>
                                            <td>{{ ++$i }}</td>
                                            
											<td>{{ $instructor->NombreInstructor }}</td>
											<td>{{ $instructor->ApellidoInstructor }}</td>
											<td>{{ $instructor->TipoDocumentoInstructor }}</td>
											<td>{{ $instructor->DocumentoInstructor }}</td>
											<td>{{ $instructor->users_id }}</td>

                                            <td>
                                                <form action="{{ route('instructor.destroy',$instructor->id) }}" method="POST">
                                                    <a class="btn btn-sm btn-primary " href="{{ route('instructor.show',$instructor->id) }}"><i class="fa fa-fw fa-eye"></i> Show</a>
                                                    <a class="btn btn-sm btn-success" href="{{ route('instructor.edit',$instructor->id) }}"><i class="fa fa-fw fa-edit"></i> Edit</a>
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-fw fa-trash"></i> Delete</button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                {!! $instructors->links() !!}
            </div>
        </div>
    </div>
@endsection

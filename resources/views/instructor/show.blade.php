@extends('layouts.app')

@section('template_title')
    {{ $instructor->name ?? 'Show Instructor' }}
@endsection

@section('content')
    <section class="content container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="float-left">
                            <span class="card-title">Show Instructor</span>
                        </div>
                        <div class="float-right">
                            <a class="btn btn-primary" href="{{ route('instructors.index') }}"> Back</a>
                        </div>
                    </div>

                    <div class="card-body">
                        
                        <div class="form-group">
                            <strong>Nombreinstructor:</strong>
                            {{ $instructor->NombreInstructor }}
                        </div>
                        <div class="form-group">
                            <strong>Apellidoinstructor:</strong>
                            {{ $instructor->ApellidoInstructor }}
                        </div>
                        <div class="form-group">
                            <strong>Tipodocumentoinstructor:</strong>
                            {{ $instructor->TipoDocumentoInstructor }}
                        </div>
                        <div class="form-group">
                            <strong>Documentoinstructor:</strong>
                            {{ $instructor->DocumentoInstructor }}
                        </div>
                        <div class="form-group">
                            <strong>Users Id:</strong>
                            {{ $instructor->users_id }}
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

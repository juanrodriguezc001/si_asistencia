@extends('layouts.app')

@section('template_title')
    {{ $actividad->name ?? 'Show Actividad' }}
@endsection

@section('content')
    <section class="content container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="float-left">
                            <span class="card-title">Show Actividad</span>
                        </div>
                        <div class="float-right">
                            <a class="btn btn-primary" href="{{ route('actividad.index') }}"> Back</a>
                        </div>
                    </div>

                    <div class="card-body">
                        
                        <div class="form-group">
                            <strong>Trimestre:</strong>
                            {{ $actividad->Trimestre }}
                        </div>
                        <div class="form-group">
                            <strong>Nombre_Actividad:</strong>
                            {{ $actividad->Nombre_Actividad }}
                        </div>
                        <div class="form-group">
                            <strong>Programa_Id:</strong>
                            {{ $actividad->Programa_Id}}
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

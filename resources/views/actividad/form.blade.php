<div class="box box-info padding-1">
    <div class="box-body">
        
        <div class="form-group">
            {{ Form::label('Trimestre') }}
            {{ Form::number('Trimestre', $actividad->Trimestre, ['class' => 'form-control' . ($errors->has('Trimestre') ? ' is-invalid' : ''), 'placeholder' => 'Trimestre']) }}
            {!! $errors->first('Trimestre', '<div class="invalid-feedback">:message</p>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('Nombre_Actividad') }}
            {{ Form::text('Nombre_Actividad', $actividad->Nombre_Actividad, ['class' => 'form-control' . ($errors->has('Nombre_Actividad') ? ' is-invalid' : ''), 'placeholder' => 'Nombre_Actividad']) }}
            {!! $errors->first('Nombre_Actividad', '<div class="invalid-feedback">:message</p>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('Programa_Id') }}
            {{ Form::number('Programa_Id', $actividad->Programa_Id, ['class' => 'form-control' . ($errors->has('Programa_Id') ? ' is-invalid' : ''), 'placeholder' => 'Programa_Id']) }}
            {!! $errors->first('users_id', '<div class="invalid-feedback">:message</p>') !!}
        </div>

    </div>
    <div class="box-footer mt20">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</div>
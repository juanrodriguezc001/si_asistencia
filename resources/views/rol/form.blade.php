<div class="box box-info padding-1">
    <div class="box-body">
        
        <div class="form-group">
            {{ Form::label('Nombre_Rol') }}
            {{ Form::text('Nombre_Rol', $rol->Nombre_Rol, ['class' => 'form-control' . ($errors->has('Nombre_Rol') ? ' is-invalid' : ''), 'placeholder' => 'Nombre_Rol']) }}
            {!! $errors->first('Nombre_Rol', '<div class="invalid-feedback">:message</p>') !!}
        </div>

    </div>
    <div class="box-footer mt20">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</div>
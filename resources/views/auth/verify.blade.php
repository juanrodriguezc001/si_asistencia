@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Verificando tu correo electronico') }}</div>

                <div class="card-body">
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            {{ __('Una verificacion ha sido enviada a tu correo electronico.') }}
                        </div>
                    @endif

                    {{ __('Despues de verifar tu correo da click a este link.') }}
                    {{ __('Si no recibiste nada en tu correo') }},
                    <form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
                        @csrf
                        <button type="submit" class="btn btn-link p-0 m-0 align-baseline">{{ __('click aqui para reenviar verificacion') }}</button>.
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

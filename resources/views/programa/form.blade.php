<div class="box box-info padding-1">
    <div class="box-body">
        
        <div class="form-group">
            {{ Form::label('Nombre_Programa') }}
            {{ Form::text('Nombre_Programa', $programa->Nombre_Programa, ['class' => 'form-control' . ($errors->has('Nombre_Programa') ? ' is-invalid' : ''), 'placeholder' => 'Nombre Programa']) }}
            {!! $errors->first('Nombre_Programa', '<div class="invalid-feedback">:message</p>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('Version_Programa') }}
            {{ Form::text('Version_Programa', $programa->Version_Programa, ['class' => 'form-control' . ($errors->has('Version_Programa') ? ' is-invalid' : ''), 'placeholder' => 'Version Programa']) }}
            {!! $errors->first('Version_Programa', '<div class="invalid-feedback">:message</p>') !!}
        </div>

    </div>
    <div class="box-footer mt20">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</div>
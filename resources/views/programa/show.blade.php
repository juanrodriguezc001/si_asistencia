@extends('layouts.app')

@section('template_title')
    {{ $programa->name ?? 'Show Programa' }}
@endsection

@section('content')
    <section class="content container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="float-left">
                            <span class="card-title">Show Programa</span>
                        </div>
                        <div class="float-right">
                            <a class="btn btn-primary" href="{{ route('programa.index') }}"> Back</a>
                        </div>
                    </div>

                    <div class="card-body">
                        
                        <div class="form-group">
                            <strong>Nombre Programa:</strong>
                            {{ $programa->Nombre_Programa }}
                        </div>
                        <div class="form-group">
                            <strong>Version Programa:</strong>
                            {{ $programa->Version_Programa }}
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

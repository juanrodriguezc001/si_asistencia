<?php

namespace App\Http\Controllers;

use App\Models\Aprendiz;
use App\Models\Asistencia;
use App\Models\Programa;
use App\Models\Instructor;
use App\Models\regAsistencia;
use Illuminate\Http\Request;

/**
 * Class tAsistenciaController
 * @package App\Http\Controllers
 */
class tAsistenciaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tAprendizs = Aprendiz::all();
        $tProgramas = Programa::all();
        $tInstructors = Instructor::all();
        /*$tAsistencias = Aprendiz::join('asistencias','asistencias.aprendizs_id','=','aprendizs.id') 
        ->select('asistencias.id as id','aprendizs.nombreAprendiz','asistencias.asiste','asistencias.fecha_asiste','aprendizs.id as idAprendiz') 
        ->get();*/
        //return $tAprendizs;
        return view('tAsistencia.index', compact('tAprendizs','tProgramas','tInstructors'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tAsistencia = new tAsistencia();
        return view('tAsistencia.create', compact('tAsistencia'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate(regAsistencia::$rules);
        $tAprendizs = Aprendiz::all();
        $tProgramas = Programa::all();
        $tInstructors = Instructor::all();

        //return request();

        return redirect()->route('tAsistencia.index', compact('tAprendizs','tProgramas','tInstructors'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tAsistencia = Aprendiz::join('asistencias','asistencias.aprendizs_id','=','aprendizs.id')  
        ->select('aprendizs.nombreAprendiz','asistencias.asiste','asistencias.fecha_asiste')
        ->where('asistencias.id','=',$id)
        ->get();
        
        return view('Asistencia.show', compact('tAsistencia'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tAsistencia = Aprendiz::join('asistencias','asistencias.aprendizs_id','=','aprendizs.id')  
        ->select('aprendizs.nombreAprendiz','asistencias.asiste','asistencias.fecha_asiste')
        ->where('asistencias.id','=',$id)
        ->get();

        return view('tAsistencia.edit', compact('tAsistencia'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  tAsistencia $tAsistencia
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Aprendiz $tAsistencia)
    {
        request()->validate(Aprendiz::$rules);

        $tAsistencia->update($request->all());

        return redirect()->route('tAsistencia.index')
            ->with('success', 'Asistencia updated successfully');
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $tAsistencia = tAsistencia::find($id)->delete();

        return redirect()->route('tAsistencia.index')
            ->with('success', 'tAsistencia deleted successfully');
    }

    function saveAttendanceData($date, $employees)
{
    deleteAttendanceDataByDate($date);
    $db = getDatabase();
    $db->beginTransaction();
    $statement = $db->prepare("INSERT INTO reg_Asistencias(employee_id, date, status) VALUES (?, ?, ?)");
    foreach ($employees as $employee) {
        $statement->execute([$employee->id, $date, $employee->status]);
    }
    $db->commit();
    return true;
}

}

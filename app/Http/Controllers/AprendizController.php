<?php

namespace App\Http\Controllers;

use App\Models\Aprendiz;
use Illuminate\Http\Request;

/**
 * Class AprendizController
 * @package App\Http\Controllers
 */
class AprendizController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $aprendizs = Aprendiz::paginate();

        return view('aprendiz.index', compact('aprendizs'))
            ->with('i', (request()->input('page', 1) - 1) * $aprendizs->perPage());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $aprendiz = new Aprendiz();
        return view('aprendiz.create', compact('aprendiz'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate(Aprendiz::$rules);

        $aprendiz = Aprendiz::create($request->all());

        return redirect()->route('aprendiz.index')
            ->with('success', 'Aprendiz created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $aprendiz = Aprendiz::find($id);

        return view('aprendiz.show', compact('aprendiz'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $aprendiz = Aprendiz::find($id);

        return view('aprendiz.edit', compact('aprendiz'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  Aprendiz $aprendiz
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Aprendiz $aprendiz)
    {
        request()->validate(Aprendiz::$rules);

        $aprendiz->update($request->all());

        return redirect()->route('aprendiz.index')
            ->with('success', 'Aprendiz updated successfully');
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $aprendiz = Aprendiz::find($id)->delete();

        return redirect()->route('aprendiz.index')
            ->with('success', 'Aprendiz deleted successfully');
    }
}

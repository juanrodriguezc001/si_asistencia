<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class tAsistencia extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tAprendizs = Aprendiz::all();
        return view('tAsistencia.index', compact('tAprendizs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tAsistencia = new tAsistencia();
        return view('tAsistencia.create', compact('tAsistencia'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate(Aprendiz::$rules);

        $tAsistencia = tAsistencia::create($request->all());

        return redirect()->route('tAsistencia.index')
            ->with('success', 'Asistencia created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tAsistencia = Aprendiz::join('asistencias','asistencias.aprendizs_id','=','aprendizs.id')  
        ->select('aprendizs.nombreAprendiz','asistencias.asiste','asistencias.fecha_asiste')
        ->where('asistencias.id','=',$id)
        ->get();
        
        return view('Asistencia.show', compact('tAsistencia'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tAsistencia = Aprendiz::join('asistencias','asistencias.aprendizs_id','=','aprendizs.id')  
        ->select('aprendizs.nombreAprendiz','asistencias.asiste','asistencias.fecha_asiste')
        ->where('asistencias.id','=',$id)
        ->get();

        return view('tAsistencia.edit', compact('tAsistencia'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  tAsistencia $tAsistencia
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Aprendiz $tAsistencia)
    {
        request()->validate(Aprendiz::$rules);

        $tAsistencia->update($request->all());

        return redirect()->route('tAsistencia.index')
            ->with('success', 'Asistencia updated successfully');
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $tAsistencia = tAsistencia::find($id)->delete();

        return redirect()->route('tAsistencia.index')
            ->with('success', 'tAsistencia deleted successfully');
    }
}

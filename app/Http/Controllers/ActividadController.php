<?php

namespace App\Http\Controllers;

use App\Models\actividad;
use Illuminate\Http\Request;

class ActividadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $actividads = actividad::paginate();

        return view('actividad.index', compact('actividads'))
            ->with('i', (request()->input('page', 1) - 1) * $actividads->perPage());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $actividad = new actividad();
        return view('actividad.create', compact('actividad'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate(actividad::$rules);

        $instructor = actividad::create($request->all());

        return redirect()->route('actividad.index')
            ->with('success', 'actividad created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\actividad  $actividad
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $actividad = actividad::find($id);

        return view('actividad.show', compact('actividad'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\actividad  $actividad
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $actividad = actividad::find($id);

        return view('actividad.edit', compact('actividad'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\actividad  $actividad
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, actividad $actividad)
    {
        request()->validate(actividad::$rules);

        $actividad->update($request->all());

        return redirect()->route('actividad.index')
            ->with('success', 'Actividad updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\actividad  $actividad
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $instructor = actividad::find($id)->delete();

        return redirect()->route('actividad.index')
            ->with('success', 'Actividad deleted successfully');
    }
}

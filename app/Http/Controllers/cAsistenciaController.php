<?php

namespace App\Http\Controllers;

use App\Models\Aprendiz;
use Illuminate\Http\Request;

/**
 * Class cAsistenciaController
 * @package App\Http\Controllers
 */
class CAsistenciaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $cAsistencias = Aprendiz::join('asistencias','asistencias.aprendizs_id','=','aprendizs.id') 
        ->select('asistencias.id as id','aprendizs.nombreAprendiz','asistencias.asiste','asistencias.fecha_asiste','aprendizs.id as idAprendiz') 
        ->get();
        //return $tAprendiz + "," +  $tAsistencias;
        return view('cAsistencia.index', compact('cAsistencias'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cAsistencia = new cAsistencia();
        return view('cAsistencia.create', compact('cAsistencia'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate(Aprendiz::$rules);

        $cAsistencia = cAsistencia::create($request->all());

        return redirect()->route('cAsistencia.index')
            ->with('success', 'cAsistencia created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $cAsistencia = Aprendiz::join('asistencias','asistencias.aprendizs_id','=','aprendizs.id')  
        ->select('aprendizs.nombreAprendiz','asistencias.asiste','asistencias.fecha_asiste')
        ->where('asistencias.id','=',$id)
        ->get();
        
        return view('cAsistencia.show', compact('cAsistencia'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cAsistencia = Aprendiz::join('asistencias','asistencias.aprendizs_id','=','aprendizs.id')  
        ->select('aprendizs.nombreAprendiz','asistencias.asiste','asistencias.fecha_asiste')
        ->where('asistencias.id','=',$id)
        ->get();

        return view('cAsistencia.edit', compact('cAsistencia'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  cAsistencia $cAsistencia
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Aprendiz $cAsistencia)
    {
        request()->validate(Aprendiz::$rules);

        $cAsistencia->update($request->all());

        return redirect()->route('cAsistencia.index')
            ->with('success', 'Asistencia updated successfully');
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $cAsistencia = cAsistencia::find($id)->delete();

        return redirect()->route('cAsistencia.index')
            ->with('success', 'cAsistencia deleted successfully');
    }
}

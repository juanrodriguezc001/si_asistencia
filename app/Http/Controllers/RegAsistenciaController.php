<?php

namespace App\Http\Controllers;

use App\Models\Aprendiz;
use App\Models\Asistencia;
use App\Models\Programa;
use App\Models\Instructor;
use App\Models\RegAsistencia;
use Illuminate\Http\Request;

/**
 * Class RegAsistenciaController
 * @package App\Http\Controllers
 */
class RegAsistenciaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $regAsistencias = RegAsistencia::paginate();

        return view('regAsistencia.index', compact('regAsistencias'))
            ->with('i', (request()->input('page', 1) - 1) * $regAsistencias->perPage());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tAprendizs = Aprendiz::all();
        $tProgramas = Programa::all();
        $tInstructors = Instructor::all();
        $regAsistencia = new RegAsistencia();
        return view('regAsistencia.create', compact('regAsistencia','tAprendizs','tProgramas','tInstructors'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        request()->validate(RegAsistencia::$rules);

        $regAsistencia = RegAsistencia::create($request->all());

        return redirect()->route('regAsistencia.index')
            ->with('success', 'RegAsistencia created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $regAsistencia = RegAsistencia::find($id);

        return view('regAsistencia.show', compact('regAsistencia'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $regAsistencia = RegAsistencia::find($id);

        return view('regAsistencia.edit', compact('regAsistencia'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  RegAsistencia $regAsistencia
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RegAsistencia $regAsistencia)
    {
        request()->validate(RegAsistencia::$rules);

        $regAsistencia->update($request->all());

        return redirect()->route('regAsistencia.index')
            ->with('success', 'RegAsistencia updated successfully');
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $regAsistencia = RegAsistencia::find($id)->delete();

        return redirect()->route('regAsistencia.index')
            ->with('success', 'RegAsistencia deleted successfully');
    }
}

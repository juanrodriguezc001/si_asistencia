<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Asistencia
 *
 * @property $id
 * @property $Nombre_Rol
 * 
 *
 * @package App
 * @mixin \Illuminate\Database\Eloquent\Builder
 */

class Rol extends Model
{
    static $rules = [
		'Nombre_Rol' => 'required',
	
    ];

    protected $perPage = 20;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['Nombre_Rol'];

}

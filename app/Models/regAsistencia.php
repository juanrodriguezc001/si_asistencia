<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class RegAsistencia
 *
 * @property $id
 * @property $NombreAprendiz
 * @property $Asistencia
 * @property $Programa
 * @property $Instructor
 * @property $created_at
 * @property $updated_at
 *
 * @package App
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class RegAsistencia extends Model
{
    
    static $rules = [
		'NombreAprendiz' => 'required',
		'Asistencia' => 'required',
		'Programa' => 'required',
		'Instructor' => 'required',
    ];

    protected $perPage = 20;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['NombreAprendiz','Asistencia','Programa','Instructor'];



}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Instructor
 *
 * @property $id
 * @property $NombreInstructor
 * @property $ApellidoInstructor
 * @property $TipoDocumentoInstructor
 * @property $DocumentoInstructor
 * @property $users_id
 * @property $created_at
 * @property $updated_at
 *
 * @package App
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class Instructor extends Model
{
    
    static $rules = [
		'NombreInstructor' => 'required',
		'ApellidoInstructor' => 'required',
		'TipoDocumentoInstructor' => 'required',
		'DocumentoInstructor' => 'required',
		'users_id' => 'required',
    ];

    protected $perPage = 20;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['NombreInstructor','ApellidoInstructor','TipoDocumentoInstructor','DocumentoInstructor','users_id'];



}

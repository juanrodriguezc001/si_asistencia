<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Aprendiz
 *
 * @property $id
 * @property $NombreAprendiz
 * @property $ApellidoAprendiz
 * @property $TipoDocumentoAprendiz
 * @property $DocumentoAprendiz
 * @property $asistencias_id
 * @property $users_id
 * @property $created_at
 * @property $updated_at
 *
 * @package App
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class Aprendiz extends Model
{
    
    static $rules = [
		'NombreAprendiz' => 'required',
		'ApellidoAprendiz' => 'required',
		'TipoDocumentoAprendiz' => 'required',
		'DocumentoAprendiz' => 'required',
		'asistencias_id' => 'required',
		'users_id' => 'required',
    ];

    protected $perPage = 20;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['NombreAprendiz','ApellidoAprendiz','TipoDocumentoAprendiz','DocumentoAprendiz','asistencias_id','users_id'];



}

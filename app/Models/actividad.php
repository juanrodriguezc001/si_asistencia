<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Instructor
 *
 * @property $id
 * @property $Nombre_Actividad
 * @property $Programa_id
 * @property $created_at
 * @property $updated_at
 *
 * @package App
 * @mixin \Illuminate\Database\Eloquent\Builder
 */

class actividad extends Model
{
    static $rules = [
		'Trimestre' => 'required',
		'Nombre_Actividad' => 'required',
		'Programa_id' => 'required',
    ];

    protected $perPage = 20;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['Trimestre','Nombre_Actividad','Programa_id'];
}

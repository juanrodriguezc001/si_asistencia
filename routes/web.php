<?php

use app\Http\Controllers\cAsistenciaController;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::resource('cAsistencia', App\Http\Controllers\cAsistenciaController::class)->middleware('auth');
Route::resource('instructor', App\Http\Controllers\InstructorController::class)->middleware('auth');
Route::resource('aprendiz', App\Http\Controllers\AprendizController::class)->middleware('auth');
Route::resource('asistencia', App\Http\Controllers\AsistenciaController::class)->middleware('auth');
Route::resource('rol', App\Http\Controllers\RolController::class)->middleware('auth');
Route::resource('actividad', App\Http\Controllers\ActividadController::class)->middleware('auth');
Route::resource('programa', App\Http\Controllers\ProgramaController::class)->middleware('auth');
Route::resource('tAsistencia', App\Http\Controllers\tAsistenciaController::class)->middleware('auth');
Route::resource('regAsistencia', App\Http\Controllers\RegAsistenciaController::class)->middleware('auth');
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

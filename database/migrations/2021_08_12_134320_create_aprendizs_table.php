<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAprendizsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aprendizs', function (Blueprint $table) {
            $table->id();

            $table->String('NombreAprendiz')->unique();
            $table->String('ApellidoAprendiz')->unique();
            $table->String('TipoDocumentoAprendiz');
            $table->String('DocumentoAprendiz')->unique();
            $table->unsignedBiginteger('asistencias_id');
            
            
            

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('aprendizs');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRegAsistenciasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reg_asistencias', function (Blueprint $table) {
            $table->id();
            $table->string('NombreAprendiz')->unique();
            $table->string('Asistencia');
            $table->string('Programa')->unique();
            $table->string('Instructor')->unique();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reg_asistencias');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFichaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ficha', function (Blueprint $table) {
            $table->id();
            $table->string('Cantidad_Aprendinces');
            $table->string('Numero_Ficha')->unique();
            $table->string('Tipo_Ficha');
            $table->date('Fecha_Inicio');
            $table->date('Fecha_Fin');
            $table->string('Estado_Ficha');
            $table->foreignId('Programa_Id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ficha');
    }
}

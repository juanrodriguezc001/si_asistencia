<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExcusaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('excusa', function (Blueprint $table) {
            $table->id();
            $table->foreignId('Aprendiz_Id');
            $table->foreignId('Ficha_Id');
            $table->string('Foto');
            $table->datetime('Fecha_Validacion');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('excusa');
    }
}
